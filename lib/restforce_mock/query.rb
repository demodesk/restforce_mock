module RestforceMock
  class Query
    attr_accessor :select, :from, :where_left, :where_right

    def initialize(query)
      self.select, self.from, self.where_left, self.where_right = self.class.parse query
    end

    def self.parse(query)
      match = query.match(/^SELECT (.*) FROM ([A-Z]+) WHERE ([A-Z]+) = ['"](.*)['"]$/i)

      if match.nil?
        raise ArgumentError, "This query cannot be mocked automatically. Please manually mock it. Query: #{query}"
      else
        match.captures
      end
    end
  end
end
