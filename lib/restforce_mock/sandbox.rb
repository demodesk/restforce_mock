require "restforce_mock/query"

module RestforceMock
  module Sandbox

    def self.add_object(name, id, values)
      if storage[name] && !storage[name][id].nil?
        raise "Object #{name} with #{id} exists"
      end
      object = {
        attributes: OpenStruct.new(type: name, url: ''),
        Id: id,
        Type: name,
        **values,
      }
      storage[name].merge!({id => object})
    end

    def add_object(name, id, values)
      RestforceMock::Sandbox.add_object(name, id, values)
    end

    def get_object(name, id)
      RestforceMock::Sandbox.get_object(name, id)
    end

    def self.get_object(name, id)
      storage[name][id]
    end

    def query_object(query)
      RestforceMock::Sandbox.run_query(query)
    end

    def self.run_query(query)
      parsed_query = RestforceMock::Query.new(query)

      storage[parsed_query.from].values.find do |object|
        object[parsed_query.where_left.to_sym] == parsed_query.where_right
      end
    end

    def self.update_object(name, id, attrs)
      current = storage[name][id]
      storage[name][id] = current.merge(attrs)
    end

    def update_object(name, id, attrs)
      RestforceMock::Sandbox.update_object(name, id, attrs)
    end

    def self.reset!
      $restforce_mock_storage = initialize
    end

    def self.storage
      $restforce_mock_storage ||= initialize
    end

    # private

    def self.update_schema(object_name)
      s = RestforceMock::SchemaManager.new
      storage[:schema][object_name] = s.get_schema(object_name)
    end

    def self.client
      ::Restforce.new
    end

    def self.initialize
      storage = Hash.new do |hash, object|
        hash[object] = {}
      end
      storage[:schema] = Hash.new do |hash, object|
        hash[object] = {}
      end
    end

    def self.validate_all_present_fields!(current, attrs)
      missing = attrs.keys - current.keys
      unless missing.length == 0
        raise Faraday::Error::ResourceNotFound,
          "INVALID_FIELD_FOR_INSERT_UPDATE: Unable to create/update fields: #{missing}." \
          " Please check the security settings of this field and verify that it is " \
          "read/write for your profile or permission set"
      end
    end

  end
end
