# frozen_string_literal: true

require 'restforce'
require 'restforce_mock/sandbox'
require 'securerandom'

module RestforceMock
  class Client
    include ::Restforce::Concerns::API
    include RestforceMock::Sandbox

    def initialize(opts={}) end

    def mashify?
      true
    end

    def api_head
    end

    def api_get(url, attrs=nil)
      url =~ %r{sobjects/(.+)/(.+)}
      object = Regexp.last_match(1)
      id = Regexp.last_match(2)

      if !attrs.nil? && url == 'query'
        query = attrs.values.first
        response = query_object(query)
        Body.new(response, url)
      elsif url == 'search'
        raise NotImplementedError
      else
        response = get_object(object, id)
        Body.new(response)
      end
    end

    def api_patch(url, attrs)
      url =~ %r{sobjects/(.+)/(.+)}
      object = Regexp.last_match(1)
      id = Regexp.last_match(2)
      validate_schema!(object)
      validate_presence!(object, id)
      update_object(object, id, attrs)
    end

    def api_post(url, attrs)
      url =~ %r{sobjects/(.+)}
      sobject = Regexp.last_match(1)
      id = Array.new(18) { [*'A'..'Z', *'0'..'9'].sample }.join
      validate_schema!(sobject)
      validate_requires!(sobject, attrs)
      add_object(sobject, id, attrs)
      Body.new({'id' => id})
    end

    def validate_requires!(sobject, attrs)
      return unless RestforceMock.configuration.schema_file
      return unless RestforceMock.configuration.error_on_required

      object_schema = schema[sobject]
      required = object_schema
          .select { |_k, v| v[:required] }
          .collect { |k, _v| k }
          .collect(&:to_sym)

      missing = required - attrs.keys - RestforceMock.configuration.required_exclusions
      unless missing.empty?
        raise Faraday::Error::ResourceNotFound, "REQUIRED_FIELD_MISSING: Required fields are missing: #{missing}"
      end
    end

    def validate_presence!(object, id)
      unless RestforceMock::Sandbox.storage[object][id]
        msg = "Provided external ID field does not exist or is not accessible: #{id}"
        raise Faraday::Error::ResourceNotFound, msg
      end
    end

    def validate_schema!(object)
      if RestforceMock.configuration.raise_on_schema_missing
        unless schema[object]
          raise RestforceMock::Error, "No schema for Salesforce object #{object}"
        end
      end
    end

    private

    def schema
      @schema ||=
          begin
            manager = RestforceMock::SchemaManager.new
            begin
              manager.load_schema(RestforceMock.configuration.schema_file)
            rescue Errno::ENOENT
              raise RestforceMock::Error, 'No schema for Salesforce object is available'
            end
          end
    end

    class Body
      attr_reader :body

      def initialize(object, type=nil)
        @body =
            if type == 'query'
              Restforce::Collection.new(collection(object), Restforce::Data::Client.new)
            else
              object
            end
      end

      def collection(object)
        {
          'totalSize' => 1,
          'done' => true,
          'records' => [
            {
              'attributes' => {
                'type' => object[:Type],
                'url' => ''
              },
              **object,
            },
          ],
        }
      end
    end
  end
end
